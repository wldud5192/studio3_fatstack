﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPurifyScript : MonoBehaviour
{
    public DialogueScript dialogue;
    public void TriggerDialogue()
    {
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
        Debug.Log("Tutorial 2nd conversation began");
    }
}
