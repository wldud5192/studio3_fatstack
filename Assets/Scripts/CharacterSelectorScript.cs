﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelectorScript : MonoBehaviour
{
    public GameObject[] characters;
    public int currentIndex = 0;
    public InputField playerNameInput;

    public void CharacterIndexUp()
    {
        currentIndex++;

        if (currentIndex > characters.Length - 1)
        {
            currentIndex = characters.Length - 1;
        }
        characterChoiceUpdate();
    }
    public void CharacterIndexDown()
    {
        currentIndex--;

        if (currentIndex < 0)
        {
            currentIndex = 0;
        }
        characterChoiceUpdate();
    }

    public void characterChoiceUpdate()
    {
        for (int i = 0; i < characters.Length; i++) {
            characters[i].GetComponent<Image>().enabled = false;
                }
        characters[currentIndex].GetComponent<Image>().enabled = true;
    }

    public void PlayGameWithChosenCharacter()
    {
        PlayerPrefs.SetString("key_playerName", playerNameInput.text);
        if (currentIndex == 0)
        {
            PlayerPrefs.SetString("key_ecomateType", "Tree");
            Debug.Log("Player has chosen to play as 'Tree'");
        }
        if (currentIndex == 1)
        {
            PlayerPrefs.SetString("key_ecomateType", "Air");
            Debug.Log("Player has chosen to play as 'Air'");
        }
        if (currentIndex == 2)
        {
            PlayerPrefs.SetString("key_ecomateType", "Water");
            Debug.Log("Player has chosen to play as 'Water'");
        }
        Debug.Log("Player set Player name as " + playerNameInput.text);
        UnityEngine.SceneManagement.SceneManager.LoadScene(2);
    }

}
