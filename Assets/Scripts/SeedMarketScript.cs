﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SeedMarketScript : MonoBehaviour
{
    public Text upgradeCostText;
    public Text upgradedIncomeRateText;
    public GameObject chooseTileCanvas;
    public GameObject correspondingHabitat;
    public static GameObject seedCanvas;

    void Start()
    {
        currentIncomeRate = correspondingHabitat.GetComponent<HabitatScript>().currentIncomeRate;
        seedCanvas = transform.parent.parent.parent.gameObject;
    }

    public int seedLevel = 1;
    public float baseUpgradeCost = 50f;
    public float upgradeCost = 50f;
    public float costMultiplier = 1.10f;
    public float incomeMultiplier = 5f;
    public float currentIncomeRate = 10f;
    public float nextIncomeRate = 100f;
    

    public void OnClickSeedPurchase()
    {
        //If Player has enough Eco-Energy,
        if (PlayerScript.ecoEnergy >= upgradeCost)
        {
            Debug.Log("Choose a tile to seed plant on.");
            chooseTileCanvas.SetActive(true);
            seedCanvas.SetActive(false);
        }
        else
        {
            Debug.Log("Cannot plant a new seed, not enough Eco-energy.");
        }
        //Else Display "Not enough Eco-energy!"

    }

    public void Planting(GameObject selectedHabitat)
    {
        correspondingHabitat = selectedHabitat;
        Debug.Log("A new seed is planted on " + correspondingHabitat.name);
        //SFX Play
        currentIncomeRate = nextIncomeRate;
        correspondingHabitat.GetComponent<HabitatScript>().currentIncomeRate = nextIncomeRate;
        PlayerScript.ecoEnergy -= upgradeCost;
        PlayerScript.playerExpenditure.Add("Seed");
        seedLevel++;
        upgradeCost = baseUpgradeCost * Mathf.Pow(costMultiplier, seedLevel);
        //nextIncomeRate = habitatLevel * incomeMultiplier;
        //upgradeCostText.text = upgradeCost.ToString("F2");
        //nextIncomeRate = nextIncomeRate + 100f;
        upgradedIncomeRateText.text = nextIncomeRate + " Eco-energy per cycle (Current: " + currentIncomeRate.ToString("F2") + ")";
        seedCanvas.SetActive(true);
    }
}
