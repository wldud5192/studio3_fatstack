﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class WorldManager : MonoBehaviour
{
    public Text worldTypeText;
    public Text worldLevelText;
    public static int worldType = 1;
    public static int worldLevel = 1;

    public GameObject[] world_level_2;
    public GameObject[] world_level_3;
    public GameObject[] world_level_4;
    public GameObject[] world_level_5;


    private void Awake()
    {
        HabitatScript.habitatUnlockCost = 100;

        if (PlayerPrefs.HasKey("key_worldLevel"))
        {
            worldType = PlayerPrefs.GetInt("key_worldType");
            worldLevel = PlayerPrefs.GetInt("key_worldLevel");
        } else
        {
            print("No world data to load");
        }
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        if (worldLevel == 2)
        {
            foreach(GameObject tile in world_level_2)
            {
                tile.SetActive(true);
            }
        }

        if (worldLevel == 3)
        {
            foreach (GameObject tile in world_level_3)
            {
                tile.SetActive(true);
            }
        }
        if (worldLevel == 4)
        {
            foreach (GameObject tile in world_level_4)
            {
                tile.SetActive(true);
            }
        }
        if (worldLevel == 5)
        {
            foreach (GameObject tile in world_level_5)
            {
                tile.SetActive(true);
            }
        }

        worldTypeText.text = worldType.ToString();
        worldLevelText.text = worldLevel.ToString();

    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ReturnHome()
    {
        Debug.Log("Player returns to scene 0");
        PlayerPrefs.SetInt("key_worldType", worldType);
        PlayerPrefs.SetInt("key_worldLevel", worldLevel);
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
