/* 
*   Pedometer
*   Copyright (c) 2018 Yusuf Olokoba
*/

namespace PedometerU.Tests {

    using UnityEngine;
    using UnityEngine.UI;

    public class StepCounter : MonoBehaviour {

        public Text stepText, distanceText;
        private Pedometer pedometer;

        public int visibleStep;
        float visibleTotalDistance;

        public float upgradableEnergyConversionResult = 100;

        private void Start () {
            // Create a new pedometer
            pedometer = new Pedometer(OnStep);
            // Reset UI
            OnStep(0, 0);

            visibleStep = PlayerPrefs.GetInt("key_step");
            visibleTotalDistance = PlayerPrefs.GetFloat("key_distance");
        }

        private void OnStep (int steps, double distance) {
            visibleStep = steps;
            visibleTotalDistance = (float)distance;
            // Display the values // Distance in feet
            stepText.text = visibleStep.ToString();
            distanceText.text = (distance * 0.001f).ToString("F2");
        }
        private void OnApplicationQuit()
        {
            PlayerPrefs.SetInt("key_step", visibleStep);
            PlayerPrefs.SetFloat("key_distance", visibleTotalDistance);
        }
        private void OnDisable () {
            // Release the pedometer
            pedometer.Dispose();
            pedometer = null;
        }
        
    }
}