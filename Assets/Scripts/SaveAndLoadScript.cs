﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveAndLoadScript : MonoBehaviour
{
    SerializationManager serializationManager;

    // Start is called before the first frame update
    void Start()
    {
        serializationManager = this.gameObject.GetComponent<SerializationManager>();
        StartCoroutine(Load());
    }

    void OnApplicationQuit()
    {
        PlayerPrefs.Save();
        serializationManager.SaveProfile();
        serializationManager.SaveProgress();
    }

    void OnApplicationPause(bool b)
    {
        if (serializationManager)
        {
            PlayerPrefs.Save();
            serializationManager.SaveProfile();
            serializationManager.SaveProgress();
        }
    }

    IEnumerator Load()
    {
        yield return new WaitForSeconds(.5f);
        serializationManager.LoadProfile();
        serializationManager.LoadProgress();
    }
}
