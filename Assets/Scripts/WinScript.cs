﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class WinScript : MonoBehaviour
{
    public SerializationManager serializationManager;
    public GameObject victoryScreen;


    public void Victory()
    {
        Victory(victoryScreen);
    }
    public static void Victory(GameObject vs)
    {
       vs.SetActive(true);
    }
    public void WinOnClick() {
        Debug.Log("Play next level...");
        WorldManager.worldLevel++;
        if(WorldManager.worldLevel > 5)
        {
            WorldManager.worldType++;
            WorldManager.worldLevel = 1;
            Debug.Log("Player has cleared the World Type! Now moving to the next stage..");
        }
        PlayerPrefs.SetInt("key_worldType", WorldManager.worldType);
        PlayerPrefs.SetInt("key_worldLevel", WorldManager.worldLevel);
        serializationManager.OnWinDeleteGameProgressOnly();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

    void Start()
    {
        GameObject[] tiles = GameObject.FindGameObjectsWithTag("Tile");
        HabitatScript.tileList.Clear();
        foreach (GameObject tile in tiles)
        {
            if (!HabitatScript.tileList.Contains(tile))
                HabitatScript.tileList.Add(tile);
        }

        if (HabitatScript.tileList.Count != 2)
        {
            HabitatScript.numOfTotalTile = HabitatScript.tileList.Count;
        }
        else
        {
            HabitatScript.numOfTotalTile = 2;
        }
        HabitatScript.numOfRemainingTile = HabitatScript.numOfTotalTile;
        Debug.Log("Number Of Total Tiles: " + HabitatScript.numOfTotalTile);
    }    
}

