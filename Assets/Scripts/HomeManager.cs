﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HomeManager : MonoBehaviour
{
    public Button continueButton;
    public Button newGameButton;
    public static bool autoSkipOn = false;
    public AudioSource loadUpSFX;
    public GameObject quitCanvas;
    public GameObject settingsCanvas;
    public GameObject playerDataToContinue;
    public GameObject newGameWarning;
    public SerializationManager serializationManager;


    private void Start()
    {
        if (PlayerPrefs.HasKey("key_playerName"))
            continueButton.interactable = true;
        else
            continueButton.interactable = false;
    }

    public void ContinueGame()
    {
        loadUpSFX.Play();
        playerDataToContinue.SetActive(true);
    }

    public void YesCreateNewGame()
    {
        serializationManager.DeleteProgress();
        SceneManager.LoadScene(1);
    }

    public void NewGame()
    {
        loadUpSFX.Play();
        newGameWarning.SetActive(true);
    }
    
    public void DontCreateNewGame()
    {
        loadUpSFX.Play();
        newGameWarning.SetActive(false);
    }

    public void LoadScene(int sceneIndex)
    {
        loadUpSFX.Play();
        SceneManager.LoadScene(sceneIndex);
    }

    public void HomeButton()
    {
        Application.Quit();
    }

    public void Settings()
    {
        settingsCanvas.SetActive(true);
    }
    public void CloseSettings()
    {
        settingsCanvas.SetActive(false);
    }


    public void QuitButton()
    {
        quitCanvas.SetActive(true);
    }

    public void NoDontQuit()
    {
        quitCanvas.SetActive(false);
    }

    public void YesQuit()
    {
        Application.Quit();
    }

    public void AutoSkipTutorial()
    {
        if (GetComponent<Toggle>().isOn)
        {
            Debug.Log("SkipOn");
            autoSkipOn = true;
        } else
        {
            Debug.Log("SkipOff");
            autoSkipOn = false;
        }
    }

    public void SoundOnOff()
    {
        if (GetComponent<Toggle>().isOn)
        {
            Camera.main.gameObject.GetComponent<AudioListener>().enabled = true;
        }
        else
        {
            Camera.main.gameObject.GetComponent<AudioListener>().enabled = false;
        }
    }
}
