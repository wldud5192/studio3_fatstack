﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public Text loadingText;
    public Slider slider;
    void Start()
    {
        StartCoroutine(LoadAsynchronously(4));
        StartCoroutine(LoadingText());
    }

    IEnumerator LoadingText()
    {
        loadingText.text = "Loading.";
        yield return new WaitForSeconds(.5f);
        loadingText.text = "Loading..";
        yield return new WaitForSeconds(.5f);
        loadingText.text = "Loading...";
        yield return new WaitForSeconds(.5f);
        loadingText.text = "Loading....";
        yield return new WaitForSeconds(.5f);
        loadingText.text = "Loading.....";
        yield return new WaitForSeconds(.5f);
        loadingText.text = "Loading......";
    }
    IEnumerator LoadAsynchronously(int sceneIndex)
    {
        yield return new WaitForSeconds(.1f);
        AsyncOperation ao = SceneManager.LoadSceneAsync(sceneIndex);
        while(!ao.isDone)
        {
            float progress = Mathf.Clamp01(ao.progress / .9f);
            slider.value = progress;
            yield return null;
        }
        


    }
}
