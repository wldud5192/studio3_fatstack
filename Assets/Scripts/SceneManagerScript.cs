﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagerScript : MonoBehaviour
{
    public AudioSource loadUpSFX;

    //If loadupSFX is null, instantiate it from resources folder as AudioSoruce (Later)

    public void OnClickLoadScene2()
    {
        loadUpSFX.Play();
        GameObject.Find("Fade").GetComponent<Animator>().enabled = false;
        GameObject.Find("Fade").GetComponent<Animator>().runtimeAnimatorController = Resources.Load("FadeToScene2") as RuntimeAnimatorController;
        GameObject.Find("Fade").GetComponent<Animator>().enabled = true;
    }

    public void OnClickLoadScene3()
    {
        loadUpSFX.Play();
        GameObject.Find("Fade").GetComponent<Animator>().enabled = false;
        GameObject.Find("Fade").GetComponent<Animator>().runtimeAnimatorController = Resources.Load("FadeToScene3") as RuntimeAnimatorController;
        GameObject.Find("Fade").GetComponent<Animator>().enabled = true;
    }


    public void OnClickLoadScene4()
    {
        loadUpSFX.Play();
        GameObject.Find("Fade").GetComponent<Animator>().enabled = false;
        GameObject.Find("Fade").GetComponent<Animator>().runtimeAnimatorController = Resources.Load("FadeToScene4") as RuntimeAnimatorController;
        GameObject.Find("Fade").GetComponent<Animator>().enabled = true;
    }

    public void OnClickLoadScene5()
    {
        loadUpSFX.Play();
        GameObject.Find("Fade").GetComponent<Animator>().enabled = false;
        GameObject.Find("Fade").GetComponent<Animator>().runtimeAnimatorController = Resources.Load("FadeToScene5") as RuntimeAnimatorController;
        GameObject.Find("Fade").GetComponent<Animator>().enabled = true;
    }

    public void ChangeScene(int sceneIndex)
    {
        Debug.Log("Changing scene to " + sceneIndex);
        SceneManager.LoadScene(sceneIndex);
    }
}
