﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

/// <summary>
/// The Data to save
/// </summary>
[Serializable]
class PlayerProfileData
{
    public string playerName;
    public string ecomateType;
    public int ecomateLevel;
    public float averageMetreWalkedPerDay;
    public float totalMetreWalked;
}

[Serializable]
class GameProgressData
{
    //public float ecoEnergy;
    public int numOfUnlockedHabitats;

    public List<Habitat> habitats = new List<Habitat>();

    public int seedLevel;
    public float upgradeCost;
    public float nextIncomeRate;

    public int researchLevel;
    public float researchCurrentCost;
    public float researchNextCost;
}

[Serializable]
class Habitat
{
    public string tileName;
    public int habitatLevel;
    public float[] pos;
    public float currentIncomeRate;
    public float habitatUnlockCost;
    public bool isLocked;
    public bool unlockable;
}

public class SerializationManager : MonoBehaviour
{
    public string playerProfileName = "playerProfile.dat";
    private string playerProfilePath;

    public string gameProgressName = "playerProgress.dat";
    private string gameProgressPath;

    [Header("Input data")]
    public PlayerProfile profile;
    public SeedMarketScript seedMarketScript;
    public ResearchScript researchScript;
    public PlantingScript plantingScript;

    /// <summary>
    /// The actual saving of the data to the disk
    /// </summary>
    /// <typeparam name="T">The type of data to save</typeparam>
    /// <param name="data">The data to save</param>
    /// <param name="path">The path to the save file</param>
    void Save<T>(ref T data, string path)
    {
        FileStream file;
        file = File.Open(path, FileMode.OpenOrCreate);
        BinaryFormatter bf = new BinaryFormatter();
        bf.Serialize(file, data);
        file.Close();
        Debug.Log("Done saving");
    }

    // Start is called before the first frame update
    void Start()
    {
        playerProfilePath = Application.persistentDataPath + "/" + playerProfileName;
        gameProgressPath = Application.persistentDataPath + "/" + gameProgressName;
        Debug.LogWarning(Application.persistentDataPath);
    }

    [ContextMenu("Save Profile")]
    public void SaveProfile()
    {
        Debug.Log("Saving profile");

        PlayerProfileData data = new PlayerProfileData()
        {
            playerName = profile.playerName,
            ecomateType = profile.ecomateType,
            ecomateLevel = profile.ecomateLevel,
            averageMetreWalkedPerDay = profile.averageMetreWalkedPerDay,
            totalMetreWalked = profile.totalMetreWalked
        };

        Save(ref data, playerProfilePath);
    }

    [ContextMenu("Save Progress")]
    public void SaveProgress()
    {
        Debug.Log("Saving progress");

        GameProgressData data = new GameProgressData()
        {
            //ecoEnergy = PlayerScript.ecoEnergy,
            numOfUnlockedHabitats = PlayerScript.numOfUnlockedHabitat,
            seedLevel = seedMarketScript.seedLevel,
            upgradeCost = seedMarketScript.upgradeCost,
            nextIncomeRate = seedMarketScript.nextIncomeRate,
            researchLevel = researchScript.researchLevel,
            researchCurrentCost = researchScript.researchCurrentCost,
            researchNextCost = researchScript.researchNextCost
        };

        var tempList = HabitatScript.tileList;
        GameObject habitat;
        for (int i = 0; i < tempList.Count; i++)
        {
            habitat = tempList[i]?.transform?.Find("Habitat 1")?.gameObject;
            if (habitat)
            {
                HabitatScript habitatScript = habitat.GetComponent<HabitatScript>();
                data.habitats.Add(new Habitat()
                {
                    tileName = habitat.transform.parent.name,
                    habitatLevel = habitatScript.habitatLevel,
                    pos = new float[3]
                    {
                        habitatScript.gameObject.transform.position.x,
                        habitatScript.gameObject.transform.position.y,
                        habitatScript.gameObject.transform.position.z
                    },
                    currentIncomeRate = habitatScript.currentIncomeRate,
                    habitatUnlockCost = HabitatScript.habitatUnlockCost,
                    unlockable = habitatScript.unlockable,
                    isLocked = habitatScript.isLocked
                });
            }
        }

        Save(ref data, gameProgressPath);
    }

    [ContextMenu("Load Profile")]
    public void LoadProfile()
    {
        Debug.Log("Loading profile");
        if (File.Exists(playerProfilePath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(playerProfilePath, FileMode.Open);
            PlayerProfileData data = (PlayerProfileData)bf.Deserialize(file);
            file.Close();

            profile.playerName = data.playerName;
            profile.ecomateType = data.ecomateType;
            profile.ecomateLevel = data.ecomateLevel;
            profile.averageMetreWalkedPerDay = data.averageMetreWalkedPerDay;
            profile.totalMetreWalked = data.totalMetreWalked;

            Debug.Log("Done");
        }
        else
            Debug.LogWarning("No game data to load");
    }

    [ContextMenu("Load Progress")]
    public void LoadProgress()
    {
        Debug.Log("Loading game progress");
        if (File.Exists(gameProgressPath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(gameProgressPath, FileMode.Open);
            GameProgressData data = (GameProgressData)bf.Deserialize(file);
            file.Close();

            //PlayerScript.ecoEnergy = data.ecoEnergy;
            PlayerScript.numOfUnlockedHabitat = data.numOfUnlockedHabitats;
            seedMarketScript.seedLevel = data.seedLevel;
            seedMarketScript.upgradeCost = data.upgradeCost;
            seedMarketScript.nextIncomeRate = data.nextIncomeRate;
            researchScript.researchLevel = data.researchLevel;
            researchScript.researchCurrentCost = data.researchCurrentCost;
            researchScript.researchNextCost = data.researchNextCost;

            var tempList = new List<GameObject>();
            foreach (GameObject tile in HabitatScript.tileList)
            {
                for (int i = 0; i < data.habitats.Count; i++)
                {
                    if (tile.name == data.habitats[i].tileName)
                    {
                        tempList.Add(tile);
                        break;
                    }
                }
            }

            StartCoroutine(UnlockHabitat(data, tempList));

            Debug.Log("Done loading progress");
        }
        else
            Debug.LogWarning("No game data to load");
    }

    IEnumerator UnlockHabitat(GameProgressData data, List<GameObject> tempList)
    {
        int numUnlocked = 0;
        int numToUnlock = 0;
        for (int i = 0; i < tempList.Count; i++)
        {
            if (!data.habitats[i].isLocked)
                numToUnlock++;
        }

        while (numToUnlock >= numUnlocked)
        {
            for (int i = 0; i < tempList.Count; i++)
            {
                GameObject habitat = tempList[i]?.transform?.Find("Habitat 1")?.gameObject;
                if (!data.habitats[i].isLocked && habitat && habitat.GetComponent<HabitatScript>().unlockable)
                {
                    habitat.GetComponentInChildren<HabitatScript>().UnlockHabitat();
                    numUnlocked++;

                    //Load flowers based on habitat level
                    plantingScript.gameObject.SetActive(true);
                    for (int j = 1; j < data.habitats[i].habitatLevel; j++)
                    {
                        plantingScript?.YesPlantHere(habitat.transform.parent);
                    }
                    plantingScript.gameObject.SetActive(false);
                }
                yield return new WaitForEndOfFrame();
            }
        }
    }

    [ContextMenu("Delete progress")]
    public void DeleteProgress()
    {
        Debug.Log("Delete all progress");
        PlayerPrefs.DeleteAll();
        File.Delete(playerProfilePath);
        File.Delete(gameProgressPath);
    }

    public void OnWinDeleteGameProgressOnly()
    {
        Debug.Log("Delete game progress");
        File.Delete(gameProgressPath);
    }
}
