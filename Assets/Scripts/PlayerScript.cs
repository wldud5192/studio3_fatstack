﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    public static int numOfUnlockedHabitat = 0;
    public static float ecoEnergy = 1000;
    public Text ecoEnergyText;
    public static List<string> playerExpenditure = new List<string>();

    private void Start()
    {
        StartCoroutine(EnsureMinimumValue());
    }
    private void Update()
    {
        ecoEnergyText.text = ecoEnergy.ToString("F0");
    }

    IEnumerator EnsureMinimumValue()
    {
        yield return new WaitForSeconds(1.1f);
        if (ecoEnergy < 100)
        {
            ecoEnergy = 100;
            Debug.Log("Player had eco-energy below 100 at start! Set to default value 100. Is there any bug?");
        }
    }

}
