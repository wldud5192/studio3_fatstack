﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasScript : MonoBehaviour
{

    public GameObject canvas;

    public void OnClickOpen()
    {
        canvas.SetActive(true);
    }

    public void OnClickClose()
    {
        canvas.SetActive(false);
    }

}
