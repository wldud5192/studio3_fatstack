﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Monetization;
using UnityEngine.UI;

public class AdScript : MonoBehaviour
{
    int rewardCount = 0;
    string placementID_rewardedVideo = "rewardedVideo";
    bool testMode = true;

    public float rewardAmount;
    public GameObject gift;
    public GameObject giftButton;
    string adID = "3383611";
    string videoAd = "video";

    public int numOfTimesGiftAppeared = 0;
    public int numOfTimesPlayerAcceptedGift = 0;

    public Button acceptAllGiftsButton;
    public GameObject giftSlot1, giftSlot2, giftSlot3;
    public GameObject premiumGiftSlot1, premiumGiftSlot2;

    // Start is called before the first frame update
    void Start()
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
            adID = "3383610";

        if (Application.platform == RuntimePlatform.Android)
            adID = "3383611";

        Monetization.Initialize(adID, testMode);
        InvokeRepeating("ShowGift", 1f, Random.Range(100, 180));
    }

    private void Update()
    {
        if (!giftSlot1.activeSelf && !giftSlot2.activeSelf && !giftSlot3.activeSelf)
        {
            acceptAllGiftsButton.interactable = false;
        } else
        {
            acceptAllGiftsButton.interactable = true;
        }
    }
    void ShowGift()
    {
        StartCoroutine("Gift");
    }

    IEnumerator Gift()
    {
        yield return new WaitForSeconds(15f);
        Debug.Log("Gift has arrived");
        numOfTimesGiftAppeared++;
        giftButton.SetActive(true);
        StackGift();
        StackPremiumGift();
    }

    //This function is triggered by button click (Gift accept button)
    public void ShowAd()
    {
        numOfTimesPlayerAcceptedGift++;
        StartCoroutine(WaitUntilPlayerFinishWatchingAd(true));
    }

    IEnumerator WaitUntilPlayerFinishWatchingAd(bool reward = false)
    {
        string placementId = reward ? placementID_rewardedVideo : "video";
        while (!Monetization.IsReady(placementId))
        {
            yield return null;
        }

        ShowAdPlacementContent ad = null;
        ad = Monetization.GetPlacementContent(placementId) as ShowAdPlacementContent;
        if (ad != null)
        {
            if (reward)
            {
                ad.Show(RewardPlayerAfterAd);
            }
        }
    }

    void RewardPlayerAfterAd (ShowResult result)
    {
        if(result == ShowResult.Finished)
        {
            rewardCount++;
            Debug.Log("Player has been given " + rewardAmount + " as ad reward.");
            PlayerScript.ecoEnergy += rewardAmount;
            PlayerScript.playerExpenditure.Add("Ad");            
        }
    }

    public void YesReceiveGift()
    {
        ShowAd();
    }

    public void NoDontReceive()
    {
        gift.SetActive(false);
    }


    public void StackGift()
    {
        if (!giftSlot1.activeSelf)
        {
            giftSlot1.SetActive(true);
        } else if (!giftSlot2.activeSelf)
        {
            giftSlot2.SetActive(true);
        } else if (!giftSlot3.activeSelf)
        {
            giftSlot3.SetActive(true);
        } else
        {
            Debug.LogWarning("All three gift slots are occupied! Player needs to accept/reject them");
        }      

        acceptAllGiftsButton.interactable = true;
    }

    public void StackPremiumGift()
    {
        float ranNum = Random.Range(0, 10);
        if (ranNum > 5)
        {
            Debug.Log("Premium gift was given this time!");
            if (!premiumGiftSlot1.activeSelf)
            {
                premiumGiftSlot1.SetActive(true);
            }
            else if (!premiumGiftSlot2.activeSelf)
            {
                premiumGiftSlot2.SetActive(true);
            }
            else
            {
                Debug.LogWarning("All two premium gift slots are occupied! Player needs to accept/reject them");
            }
        }
        else
        {
            Debug.Log("No premium gift was given this time.");
        }
    }

    public void OnGiftCanvasCloseCheckRemainingGifts()
    {
        if (!giftSlot1.activeSelf)
        {
            if (!giftSlot2.activeSelf)
            {
                if (!giftSlot3.activeSelf)
                {
                    if (!premiumGiftSlot1.activeSelf)
                    {
                        if (!premiumGiftSlot2.activeSelf)
                        {
                            Debug.Log("No gift is left in player's gift canvas. Removing 'Gift Awaits' Button.");
                            giftButton.SetActive(false);
                        }
                    }
                }
            }
        }

    }

}