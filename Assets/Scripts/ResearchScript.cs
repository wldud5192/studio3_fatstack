﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PedometerU.Tests;
public class ResearchScript : MonoBehaviour
{
    public StepCounter stepCounter;
    public float researchCurrentCost = 50f;
    public float researchNextCost;
    float researchBaseCost = 50f;
    public int researchLevel = 0;
    float costMultiplier = 1.2f;

    public Text researchCostText_1;

    public Text energyConversionPer100StepsText;
    public void OnClickResearch1()
    {
        if (PlayerScript.ecoEnergy >= researchCurrentCost)
        {
            PlayerScript.playerExpenditure.Add("Research");
            PlayerScript.ecoEnergy -= researchCurrentCost;
            researchLevel++;
            HabitatScript.incomeSpeedBoost = Mathf.Pow(1.1f, researchLevel);
            researchNextCost = (researchBaseCost + 50) * Mathf.Pow(costMultiplier, researchLevel);
            researchCurrentCost = researchNextCost;
            researchCostText_1.text = researchNextCost.ToString("F0");
            Debug.Log("Research upgraded. Research cost: " + researchCurrentCost + " -> " + researchNextCost + ", Research level: " + researchLevel + " -> " + (researchLevel + 1).ToString() + ".");
        }
    }

    public void OnClickResearch2()
    {
        if (PlayerScript.ecoEnergy >= researchCurrentCost)
        {
            PlayerScript.playerExpenditure.Add("Research");
            PlayerScript.ecoEnergy -= researchCurrentCost;
            researchLevel++;
            researchNextCost = (researchBaseCost + 50) * Mathf.Pow(1.06f, researchLevel);
            researchCurrentCost = researchNextCost;
            researchCostText_1.text = researchNextCost.ToString("F0");
            stepCounter.upgradableEnergyConversionResult += 100;
            energyConversionPer100StepsText.text = "100 steps = " + stepCounter.upgradableEnergyConversionResult + " Eco - energy";
            Debug.Log("Research upgraded. Research cost: " + researchCurrentCost + " -> " + researchNextCost + ", Research level: " + researchLevel + " -> " + (researchLevel + 1).ToString() + ".");
            Debug.Log("Research Type: Energy Conversion Upgrade. Upgraded amount = " + stepCounter.upgradableEnergyConversionResult + " per 100 steps.");
        }
    }
}
