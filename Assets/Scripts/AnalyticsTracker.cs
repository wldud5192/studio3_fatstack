﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;

public class AnalyticsTracker : MonoBehaviour
{
    public enum LevelPlayState { InProgress, Won, Quit }
    Scene currentScene;
    LevelPlayState state = LevelPlayState.InProgress;
    public float timeElapsed = 0;


    private void Awake()
    {
        currentScene = SceneManager.GetActiveScene();
        AnalyticsEvent.LevelStart(currentScene.name, currentScene.buildIndex);
    }

    public void SetLevelPlayState(LevelPlayState newState)
    {
        this.state = newState;
    }

    private void Update()
    {
        timeElapsed += Time.deltaTime;
    }

    private void OnDestroy()
    {
        timeElapsed = 0;
    }
    /*
    private void OnDestroy()
    {
        Dictionary<string, object> customParams = new Dictionary<string, object>();
        customParams.Add("seconds_played", timeElapsed);
        switch (this.state)
        {
            case LevelPlayState.Won:
                AnalyticsEvent.LevelComplete(currentScene.name, currentScene.buildIndex, customParams);
                break;
            case LevelPlayState.InProgress:
            case LevelPlayState.Quit:
            default:
                AnalyticsEvent.LevelQuit(currentScene.name, currentScene.buildIndex, customParams);
                break;
        }
    }
    */
}
