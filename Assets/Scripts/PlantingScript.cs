﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantingScript : MonoBehaviour
{
    Transform tilePosition;
    bool selected = false;
    public GameObject flower;
    public GameObject selectionBorder;
    public GameObject playerConfirm;
    public SeedMarketScript seedMarketScript;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GetTileAtMousePosition();
        }
    }

    public Transform GetTileAtMousePosition()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit) && !selected)
        {
            Debug.Log("Mouse clicked " + hit.collider.gameObject.name);
            if (hit.collider.gameObject.tag == "Tile")
            {
                if (hit.collider.gameObject.GetComponentInChildren<HabitatScript>().isLocked == false)
                {
                    Destroy(GameObject.Find("TileSelection"));
                    GameObject border = Instantiate(selectionBorder, hit.collider.gameObject.transform.position, Quaternion.identity);
                    border.name = "TileSelection";
                    tilePosition = hit.collider.gameObject.transform;
                    Debug.Log("Tile Selected");
                    playerConfirm.SetActive(true);
                    selected = true;
                    return hit.collider.gameObject.transform;
                }
                else
                {
                    Debug.Log("The selected tile must be purified first in order to plant seed!");
                }
            }
        }
        return null;
    }

    public void YesPlantHere()
    {
        Instantiate(flower, tilePosition.position, Quaternion.identity);
        tilePosition.Find("Habitat 1").GetComponent<HabitatScript>().habitatLevel++;
        playerConfirm.SetActive(false);
        seedMarketScript.Planting(tilePosition.Find("Habitat 1").gameObject);
        Destroy(GameObject.Find("TileSelection"));
        selected = false;
        SeedMarketScript.seedCanvas.SetActive(false);
        this.gameObject.SetActive(false);
    }

    public void YesPlantHere(Transform tileTransform)
    {
        GameObject plant = Instantiate(flower, tileTransform.position, Quaternion.identity, tileTransform);
        plant.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        tileTransform.GetComponentInChildren<HabitatScript>().habitatLevel++;
    }

    public void NoDontPlantHere()
    {
        Destroy(GameObject.Find("TileSelection"));
        playerConfirm.SetActive(false);
        this.gameObject.SetActive(false);
        selected = false;
    }

    public void CancelPlanting()
    {
        Destroy(GameObject.Find("TileSelection"));
        SeedMarketScript.seedCanvas.SetActive(false);
        this.gameObject.SetActive(false);

    }


}
