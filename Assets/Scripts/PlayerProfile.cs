﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using PedometerU.Tests;
public class PlayerProfile : MonoBehaviour
{
    public StepCounter stepCounter;
    //To assign UIs in Unity Inspector

    //Player Profile to be saved & loaded
    public Text playerNameText;
    public string playerName = "Avacado";

    public Text playerLevelText;
    public int ecomateLevel = 1;

    public Image ecomateIcon;
    public string ecomateType = "Water";

    public Text todaysDistanceText;
    public float todaysMetreWalked = 0;

    public Text todaysStepsText;
    public float todaysSteps;

    public Text todaysEcoenergyText;
    public float todaysEcoenergy;

    public Text averageDistanceWalkedPerDayText;
    public float averageMetreWalkedPerDay = 0;

    public Text totalDistanceWalkedText;
    public float totalMetreWalked = 0;

    //These are derived from PlayerScript.cs
    public static float ecoEnergy = 1000;
    public static int numOfUnlockedHabitat;

    //These are derived from HabitatScript.cs
    //Each unlocked tile has its own HabitatScript.cs
    [HideInInspector]
    public int habitatLevel;

    [HideInInspector]
    public float baseIncomeRate;

    [HideInInspector]
    public float currentIncomeRate;

    static float habitatUnlockCost;

    //These are derived from SeedMarketScript.cs
    [HideInInspector]
    public int seedLevel;
    [HideInInspector]
    public float upgradeCost;
    [HideInInspector]
    public float nextIncomeRate;

    //These are derived from ResearchScript.cs
    [HideInInspector]
    public int researchLevel = 0;

    [HideInInspector]
    public float researchCurrentCost;

    [HideInInspector]
    public float researchNextCost;

    public float elapsedTime;

    private void Start()
    {
        //Update Profile every 10 seconds repeatedly
        InvokeRepeating("UpdateProfileEvery10Seconds", 1, 10);
    }
    void UpdateProfileEvery10Seconds()
    {
        //Set Player Name
        if (PlayerPrefs.HasKey("key_playerName"))
        {
            playerName = PlayerPrefs.GetString("key_playerName");
            playerNameText.text = playerName;
        }

        //Set Ecomate Type
        if (PlayerPrefs.HasKey("key_ecomateType"))
        {
            if (PlayerPrefs.GetString("key_ecomateType") == "Tree")
            {
                ecomateType = "Tree";
                ecomateIcon.sprite = Resources.Load<Sprite>("tree");
            }
            else if (PlayerPrefs.GetString("key_ecomateType") == "Air")
            {
                ecomateType = "Air";
                ecomateIcon.sprite = Resources.Load<Sprite>("air");
            }
            else if (PlayerPrefs.GetString("key_ecomateType") == "Water")
            {
                ecomateType = "Water";
                ecomateIcon.sprite = Resources.Load<Sprite>("water");
            }
        }

        //Set Level
        if (PlayerPrefs.HasKey("key_playerLevel"))
        {
            ecomateLevel = PlayerPrefs.GetInt("key_playerLevel");
            playerLevelText.text = ecomateLevel.ToString();
        }
    }

    private void Update()
    {
        MeasureElapsedTime();

        UpdateProfileEveryFrame();

    }

    void MeasureElapsedTime()
    {
        elapsedTime = PlayerPrefs.GetFloat("key_elapsedTime", 1);
        elapsedTime += Time.deltaTime;
        PlayerPrefs.SetFloat("key_elapsedTime", elapsedTime);
    }

    void UpdateProfileEveryFrame()
    {
        todaysMetreWalked = float.Parse(stepCounter.distanceText.text);
        if (PlayerPrefs.HasKey("key_totalDistance"))
        {
            totalMetreWalked = totalMetreWalked + PlayerPrefs.GetFloat("key_totalDistance", 0);
        }
        else
        {
            totalMetreWalked = todaysMetreWalked;
        }
        PlayerPrefs.SetFloat("key_totalDistance", totalMetreWalked);
        averageMetreWalkedPerDay = totalMetreWalked / elapsedTime;

    }
        /*
        if (24 hours have passed)
        {
            ResetTodaysRecord();
        }
    }

    void ResetTodaysRecord()
    {
        todaysMetreWalked = 0;
        todaysEcoeregy = 0;
        todaysSteps = 0;

    }
    */
}


