﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class ContinueGameScript : MonoBehaviour
{
    public Text worldTypeText;
    public Text worldLevelText;
    public Text playerNameText;
    public GameObject returnToMenu;
    private void Start()
    {
        if (PlayerPrefs.HasKey("key_playerName"))
        {
            playerNameText.text = PlayerPrefs.GetString("key_playerName").ToString();
        }
        if (PlayerPrefs.GetInt("key_worldType") >= 1)
        {
            worldTypeText.text = PlayerPrefs.GetInt("key_worldType").ToString();
            worldLevelText.text = PlayerPrefs.GetInt("key_worldLevel").ToString();
        }
    }
    public void YesThenContinueToGame()
    {
        SceneManager.LoadScene(2);
    }

    public void NoThenCreateNewGame()
    {
        returnToMenu.SetActive(true);
    }

    public void CloseThis()
    {
        returnToMenu.SetActive(false);
        this.gameObject.SetActive(false);
    }
}
