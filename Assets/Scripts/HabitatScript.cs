﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HabitatScript : MonoBehaviour
{
    public static List<GameObject> tileList = new List<GameObject>();
    public static int numOfTotalTile = 2;
    public static int numOfRemainingTile = 2;
    public static float incomeSpeedBoost = 1f;

    static Animator walkingAnimator;

    [SerializeField]
    GameObject habitatUnlockVFX;

    public int habitatLevel = 1;
    public static float habitatUnlockCost = 100;
    float idleIncomeSpeed = 0.1f;
    float walkIncomeSpeed = 0.3f;
    public float currentIncomeRate = 50;

    public GameObject incomeTextPrefab;
    Button habitatUnlockUI;
    public GameObject nextHabitatComponents;
    [SerializeField]
    Slider habitatBarUI;
    public Image habitatBarUIColour;
    public bool isLocked = true;

    //Pedometer Effects
    static float counter = 0;
    static float value1, value2;
    public Text distanceText;
    public static bool playerHasWalked = false;

    public bool unlockable = true;

    TileScript baseTileScript;
    

    private void Start()
    {
        //walkingAnimator = GameObject.FindGameObjectWithTag("Walking_Indicator").GetComponent<Animator>();        
        habitatUnlockUI = gameObject.GetComponentInChildren<Button>();
        habitatBarUI = gameObject.GetComponentInChildren<Slider>();
        habitatBarUI.gameObject.SetActive(false);
        baseTileScript = transform.parent.transform.GetComponent<TileScript>();

    }

    //UNLOCK
    public void ClickToUnlock()
    {

        //If Player has enough Eco-energy to unlock a Habitat!
        if (PlayerScript.ecoEnergy >= habitatUnlockCost && unlockable)
        {
            Debug.Log("Player has unlocked a new habitat. Cost: " + habitatUnlockCost);

            UnlockHabitat();
            PlayerScript.numOfUnlockedHabitat++;
            PlayerScript.ecoEnergy -= habitatUnlockCost;
            //SFX Play
        }
        else
        {
            GameObject.FindGameObjectWithTag("NextCost").GetComponent<Animator>().Play("NextCost_NotEnoughEnergy");
            Debug.Log("Not enough eco-energy to purify!");
        }
    }

    public void UnlockHabitat() 
    {
        PlayerScript.playerExpenditure.Add("Habitat");
        habitatBarUI.gameObject.SetActive(true);
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Animator>().Play("CameraAnim_Level1", -1, 0f);
        transform.parent.transform.Find("DeadHabitat").GetComponent<Animator>().Play("DeadGroundToLife", -1, 0f);
        RemainingTileCheck();
        unlockable = false;
        if (baseTileScript)
        {
            baseTileScript.UnlockSurroundingTilesInRadius();
        }
        else
        {
            int dummy = 0;
        }
        foreach (Transform nextHabitatTile in TileScript.unlockableTiles)
        {
            if (nextHabitatTile.Find("Habitat 1") || (nextHabitatTile.Find("Habitat 1(Clone)") || (nextHabitatTile.Find("Habitat 1(Clone)(Clone)") || (nextHabitatTile.Find("Habitat 1(Clone)(Clone)(Clone)")))))
            {
                //Do nothing on duplicates
            }
            else
            {
                GameObject nextHabitat = Instantiate(nextHabitatComponents, nextHabitatTile.position, Quaternion.identity);
                nextHabitat.GetComponent<HabitatScript>().unlockable = true;
                nextHabitat.name = "Habitat 1";
                nextHabitat.transform.parent = nextHabitatTile;
                nextHabitat.transform.localPosition = transform.localPosition;
                nextHabitat.transform.localScale = transform.localScale;
           }
        }
        Instantiate(habitatUnlockVFX, gameObject.transform);
        isLocked = false;
        transform.parent.transform.Find("DeadHabitat").GetComponent<Animator>().Play("GreeningUnlockedGround", -1, habitatBarUI.normalizedValue);
        if (habitatUnlockUI != null && habitatUnlockUI.gameObject != null)
            Destroy(habitatUnlockUI.gameObject);

        habitatUnlockCost = (habitatUnlockCost + 20f) * Mathf.Pow(1.2f, (float)WorldManager.worldType);
        GameObject.Find("Canvas/EcoEnergy/Next Purifying Cost").GetComponent<Text>().text = habitatUnlockCost.ToString("F0");
        Debug.Log("Successfully unlocked. Next Purifying Cost: " + habitatUnlockCost);
    }

    void RemainingTileCheck()
    {
        numOfRemainingTile -= 1;
        Debug.Log("Number of remaining tiles: " + numOfRemainingTile);
        if (numOfRemainingTile == 0)
        {
            GameObject.Find("Victory Canvas").GetComponent<WinScript>().Victory();
        }
    }
    public void Update()
    {
        if (!isLocked)
        {
            if (habitatBarUI.value == habitatBarUI.maxValue)
            {
                GameObject newIncomeText = Instantiate(incomeTextPrefab, this.gameObject.transform.Find("Habitat Canvas").transform);
                //Make it destroy itself
                newIncomeText.GetComponent<Text>().enabled = true;
                newIncomeText.GetComponent<Animator>().enabled = true;
                newIncomeText.GetComponent<HabitatIncomeText>().income = currentIncomeRate;
                PlayerScript.ecoEnergy += currentIncomeRate;
                habitatBarUI.value = 0;
            }


            if(!playerHasWalked){
                habitatBarUI.value += Time.deltaTime * idleIncomeSpeed * incomeSpeedBoost; 
            }        
            else
            {
                habitatBarUI.value += Time.deltaTime * walkIncomeSpeed * incomeSpeedBoost;
            }

            //Check if the player walked in the past 3 seconds
            counter += Time.deltaTime;

            if (counter < 1f)
            {
                if (distanceText.text != null)
                {
                    value1 = float.Parse(distanceText.text);
                }
            }

            if (counter >= 3f)
            {
                AccelerateEcoProduction(distanceText);
                counter = 0;
            }
            

        }
    }

    public static void AccelerateEcoProduction(Text dist)
    {
        value2 = float.Parse(dist.text);

        if (value2 > value1)
        {
            //walkAnimator.speed = 2f;
            //Animate the bar filling up like it's boosting!
            playerHasWalked = true;

        }
        if (value2 == value1)
        {
           // walkAnimator.speed = 1f;
            playerHasWalked = false;

        }
        value2 = 0;
        value1 = 0;
        counter = 0;

    }

    //Called on Slider Value Change() in inspector
    public void ChagneSliderColour()
    {
        habitatBarUIColour.color = Color.Lerp(Color.yellow, Color.green, habitatBarUI.value / habitatBarUI.maxValue);
    }
}
