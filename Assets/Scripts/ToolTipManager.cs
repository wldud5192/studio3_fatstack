﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolTipManager : MonoBehaviour
{
    public GameObject pinchToZoom;

    private void Start()
    {
        StartCoroutine(DIsplayToolTip(pinchToZoom));
    }

    public static IEnumerator DIsplayToolTip(GameObject toolTip)
    {
        toolTip.SetActive(true);
        yield return new WaitForSeconds(13f);
        toolTip.SetActive(false);
    }
}
