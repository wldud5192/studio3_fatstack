﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HabitatIncomeText : MonoBehaviour
{
    public float income;
    Text incomeText;
    public void DestroyText()
    {
        Destroy(this.gameObject);
    }
    void Start()
    {
        incomeText = gameObject.GetComponent<Text>();
        incomeText.text = "+ " + income.ToString();
    }
}
