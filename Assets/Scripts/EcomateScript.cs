﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EcomateScript : MonoBehaviour
{
    public Text playerNameText;
    public Image ecomateImage;
    public Sprite[] ecomateImages;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("key_ecomateType"))
        {
            if (PlayerPrefs.GetString("key_ecomateType") == "Tree")
            {
                ecomateImage.sprite = Resources.Load<Sprite>("tree");
            }
            else if (PlayerPrefs.GetString("key_ecomateType") == "Air")
            {
                ecomateImage.sprite = Resources.Load<Sprite>("air");
            }
            else if (PlayerPrefs.GetString("key_ecomateType") == "Water")
            {
                ecomateImage.sprite = Resources.Load<Sprite>("water");
            }
        }

        if (PlayerPrefs.HasKey("key_playerName"))
        {
            playerNameText.text = "Hi, " + PlayerPrefs.GetString("key_playerName") + "!";
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
