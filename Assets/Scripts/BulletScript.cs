﻿using UnityEngine;
using System.Collections;

using UnityEngine.SceneManagement;
using UnityEngine.UI;
// This script moves the character controller forward
// and sideways based on the arrow keys.
// It also jumps when pressing space.
// Make sure to attach a character controller to the same game object.
// It is recommended that you make only one call to Move or SimpleMove per frame.

public class BulletScript : MonoBehaviour
{
    CharacterController characterController;

    public float health = 100;
    public Slider healthBar;
    public GameObject bullet;
    public Transform bulletPoint;
    CharacterController charCont;

    public float speed = 6.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;

    private Vector3 moveDirection = Vector3.zero;

    void Start()
    {
        charCont = GetComponent<CharacterController>();
    }

    void Update()
    {
        if (charCont.isGrounded)
        {
            // We are grounded, so recalculate
            // move direction directly from axes

            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            moveDirection *= speed;

            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpSpeed;
            }
        }

        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)
        moveDirection.y -= gravity * Time.deltaTime;

        // Move the controller
        charCont.Move(moveDirection * Time.deltaTime);


        if (Input.GetMouseButtonDown(0))
        {
            GameObject bulletClone = Instantiate(bullet, bulletPoint.position, transform.rotation);
            bulletClone.GetComponent<Rigidbody>().velocity = transform.right * -10f;
        }
    }


    public void Restart()
    {
        if (healthBar.value <= 0)
        {
            StartCoroutine(Re());
        }
    }

    IEnumerator Re()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene(0);
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Bullet")
        {
            health -= 10f;
            healthBar.value = health;
            Destroy(other.gameObject);
        }
    }
}