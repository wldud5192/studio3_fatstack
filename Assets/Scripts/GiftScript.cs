﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GiftScript : MonoBehaviour
{
    public bool premiumGift = false;

    public int giftAmount = 15;
    public Text giftDescriptionText;
    public string giftDescription;
    public bool giftSlotOpen = false;
    public Button acceptAllGiftsButton;

    int ranNum;

    public void Start()
    {
        ranNum = Random.Range(0, 5);
    }
    public void OnEnable()
    {
        //Set random amount of ecoenergy in the gift and set its description
        if (premiumGift)
        {
            giftSlotOpen = true;            
            switch (ranNum)
            {
                case 0:
                    giftAmount = 1000;
                    giftDescription = "Eco-energy + " + giftAmount;
                    break;
                case 1:
                    giftAmount = 1200;
                    giftDescription = "Eco-energy + " + giftAmount;
                    break;
                case 2:
                    giftAmount = 1999;
                    giftDescription = "Eco-energy + " + giftAmount;
                    break;
                case 3:
                    giftAmount = 2500;
                    giftDescription = "Eco-energy + " + giftAmount;
                    break;
                case 4:
                    giftAmount = 800;
                    giftDescription = "Eco-energy + " + giftAmount;
                    break;
                case 5:
                    giftAmount = 1350;
                    giftDescription = "Eco-energy + " + giftAmount;
                    break;
            }
        }
        else
        {
            giftSlotOpen = true;
            switch (ranNum)
            {
                case 0:
                    giftAmount = 99;
                    giftDescription = "Eco-energy + " + giftAmount;
                    break;
                case 1:
                    giftAmount = 15;
                    giftDescription = "Eco-energy + " + giftAmount;
                    break;
                case 2:
                    giftAmount = 50;
                    giftDescription = "Eco-energy + " + giftAmount;
                    break;
                case 3:
                    giftAmount = 70;
                    giftDescription = "Eco-energy + " + giftAmount;
                    break;
                case 4:
                    giftAmount = 25;
                    giftDescription = "Eco-energy + " + giftAmount;
                    break;
                case 5:
                    giftAmount = 40;
                    giftDescription = "Eco-energy + " + giftAmount;
                    break;

            }
        }
        giftDescriptionText.text = giftDescription;
    }

    public void OnGiftAcceptClick()
    {
        Debug.Log("Player Has Accepted Gift: " + giftDescription);
        PlayerScript.ecoEnergy += giftAmount;
        gameObject.SetActive(false);
    }    

    public void OnClickGiftAcceptAll()
    {
        GiftScript[] gifts = FindObjectsOfType<GiftScript>();
        foreach (GiftScript gift in gifts)
        {
            if(!gift.premiumGift)
            gift.OnGiftAcceptClick();
        }
        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        giftSlotOpen = false;
    }

    public void PassRewardAmount()
    {
        AdScript ad = gameObject.transform.parent.parent.gameObject.GetComponent<AdScript>();
        ad.rewardAmount = giftAmount;
    }

    public void RejectGift()
    {
        this.gameObject.SetActive(false);
    }
}
