﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public Animator tutorialAnim;
    public Text nameText;
    public Text dialogueText;

    Queue<string> sentences;

    private void Start()
    {
        sentences = new Queue<string>();
    }
    public void StartDialogue(DialogueScript dialogue)
    {
        tutorialAnim.SetBool("IsOpen", true);
        Debug.Log("Starting tutorial dialgoue with + " + dialogue.name);
        nameText.text = dialogue.name;
        sentences.Clear();
        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();
        dialogueText.text = sentence;
    }

    void EndDialogue()
    {
        tutorialAnim.SetBool("IsOpen", false);
        Debug.Log("End Of Convo");
    }
}
