﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileScript : MonoBehaviour
{
    public static List<Transform> unlockableTiles = new List<Transform>();

    // Start is called before the first frame update
    void Start()
    {
    }


    public void UnlockSurroundingTilesInRadius()
    {
        Collider[] surroundingObjects;
        surroundingObjects = Physics.OverlapSphere(transform.position, 12f);
        unlockableTiles.Clear();
        foreach (Collider surroundingObject in surroundingObjects)
        {
            //Add all Tiles within the tile's radius
            if (surroundingObject.gameObject.tag == "Tile")
            {
                {
                    unlockableTiles.Add(surroundingObject.gameObject.transform);
                }
            }
            
            //Remove itself from the list
                if (surroundingObject == this.gameObject.GetComponent<Collider>())
                {
                    unlockableTiles.Remove(surroundingObject.gameObject.transform);
                }
        }

    }
}
