﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteDataButton : MonoBehaviour
{
    public GameObject deleteDataCanvas;
    public SerializationManager serializationManager;

    public void deleteDataButtonPressed()
    {
        deleteDataCanvas.SetActive(true);
    }
    public void YesDelete()
    {
        deleteDataCanvas.SetActive(false);
        serializationManager.DeleteProgress();
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
    public void NoDontDelete()
    {
        deleteDataCanvas.SetActive(false);
    }

    public void CloseDeleteDataCanvas()
    {
        deleteDataCanvas.SetActive(false);
    }
}
