﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PedometerU.Tests;
public class EnergyConversionScript : MonoBehaviour
{
    public StepCounter stepCounter;
    public Text stepText;
    int step;

    public void OnClickConvert()
    {

        if (int.TryParse(stepText.text, out int result))
        {
            step = result;
        }

        //Reward per 100 steps
        if (step > 100)
        {
            Debug.Log("Player converted 100 steps into eco-energy. Amount: " + stepCounter.upgradableEnergyConversionResult);
            PlayerScript.ecoEnergy += stepCounter.upgradableEnergyConversionResult;
            step -= 100;
            stepCounter.visibleStep -= 100;
            stepText.text = step.ToString();
        } else
        {
            Debug.Log("Not enough steps to convert!");
        }
    }
}
