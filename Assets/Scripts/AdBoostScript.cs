﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdBoostScript : MonoBehaviour
{

    public void AdBoost()
    {
        PlayerScript.playerExpenditure.Add("Ad");
        PlayerScript.ecoEnergy += 100f;
    }
}
