﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementScript : MonoBehaviour
{
    public GameObject profilePanel;
    public GameObject achievementPanel;

    public void openAchievementPanel()
    {
        profilePanel.SetActive(false);
        achievementPanel.SetActive(true);
    }
    

    public void openProfilePanel()
    {
        profilePanel.SetActive(true);
        achievementPanel.SetActive(false);
    }
}
