﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public GameObject tutorialTrigger;
    public DialogueScript dialogue;

    public void TriggerDialogue ()
    {
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
        Debug.Log("Tutorial began");
        if (tutorialTrigger != null)
        {
            Destroy(tutorialTrigger);
        }
    }


    public void TutorialBegin()
    {
        if (TutorialManager.playerInTutorial_Part1)
        {
            TriggerDialogue();
            ToolTipManager.DIsplayToolTip(GameObject.FindObjectOfType<ToolTipManager>().pinchToZoom);
        } else
        {
            Destroy(tutorialTrigger);
        }
    }
}
