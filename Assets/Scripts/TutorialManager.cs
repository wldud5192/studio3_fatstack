﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class TutorialManager : MonoBehaviour
{
    public AudioSource loadUpSFX;
    bool activeOnce = false;

    public static bool playerInTutorial_Part1 = true;
    public static bool playerInTutorial_Part2 = true;
    public static bool playerInTutorial_Part3 = true;


    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (SceneManager.GetActiveScene().name == "0.Start Screen" && activeOnce == false)
            {
                loadUpSFX.Play();
                GameObject.Find("Fade").GetComponent<Animator>().enabled = true;
                activeOnce = true;
            }
            
        }
    }    

    public void ChangeScene(int sceneIndex)
    {
        Debug.Log("Changing scene to " + sceneIndex);
        SceneManager.LoadScene(sceneIndex);
    }
    
    
}
