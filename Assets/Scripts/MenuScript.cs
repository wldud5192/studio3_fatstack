﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuScript : MonoBehaviour
{

    public GameObject menuComponent;

    public void OpenMenu()
    {
        menuComponent.GetComponent<Animator>().SetBool("Opening", true);
        menuComponent.SetActive(true);
    }

    public void CloseMenu()
    {
        StartCoroutine(WaitForAnim());
    }

    IEnumerator WaitForAnim()
    {
        menuComponent.GetComponent<Animator>().Play("MenuClose");
        yield return new WaitForSeconds(0.1f);
        menuComponent.SetActive(false);
    }


    public void OpenGifts()
    {
        menuComponent.SetActive(true);        
    }

}
